from card import Card 
from hand import Hand 

card_one = Card("clubs", 4, "Four")
card_two = Card("clubs", 4, "Five")

my_hand = Hand()
my_hand.add_card(card_one)
my_hand.add_card(card_two)

print(my_hand)