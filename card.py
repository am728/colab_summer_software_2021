class Card: 
    def __init__(self, suit, value, face_value): 
        self.suit = suit
        self.value = value
        self.face_value = face_value

    def __repr__(self):
        return f"{self.face_value} of {self.suit}"

    def __str__(self):
        return f"{self.face_vale} of {self.suit}"



